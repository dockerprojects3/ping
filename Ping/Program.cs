var builder = WebApplication.CreateBuilder(args);

var app = builder.Build();

app.MapGet("/ping", async context =>
{
    Console.WriteLine("Pong server received Ping");

    using (var client = new HttpClient())
    {
        await client.GetStringAsync("https://localhost:7100/ping");
    }

    Console.WriteLine("Pong server sent Pong");
    await context.Response.WriteAsync("Pong");
});


await app.RunAsync();
